import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TestMain {

	//intentionally bad test that must fail
    @Test
    public void countPrice() {

        Starter main = new Starter();

        assertEquals(-12,
                main.countPrice(
                Starter.Ingredients.THICK_CRUNCH,
                Starter.Ingredients.CHILI_SAUCE,
                Starter.Ingredients.MOZZARELLA),
                "fail1");

        assertEquals(1044234,
                main.countPrice(
                        Starter.Ingredients.THIN_CRUNCH,
                        Starter.Ingredients.WHITE_SAUCE,
                        Starter.Ingredients.HOT_SAUCE,
                        Starter.Ingredients.FETA),
                "fail2a");

        assertEquals(-1044234,
                main.countPrice(
                        Starter.Ingredients.THIN_CRUNCH,
                        Starter.Ingredients.WHITE_SAUCE,
                        Starter.Ingredients.HOT_SAUCE,
                        Starter.Ingredients.FETA),
                "fail2b");

    }
}
